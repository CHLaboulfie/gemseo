API changes:

- :meth:`.Scenario.xdsmize`

  - Rename ``latex_output`` to ``save_pdf``.
  - Rename ``html_output`` to ``save_html``.
  - Rename ``json_output`` to ``save_json``.
  - Rename ``open_browser`` to ``show_html``.
  - Rename ``outfilename`` to ``file_name`` and do not use suffix.
  - Rename ``outdir`` to ``directory_path``.

- :class:`.XDSMizer`

  - :class:`~.XDSMizer`
      - Rename :attr:`~.XDSMizer.outdir` to :attr:`~.XDSMizer.directory_path`.
      - Rename :attr:`~.XDSMizer.outfilename` to :attr:`~.XDSMizer.json_file_name`.
      - Rename :attr:`~.XDSMizer.latex_output` to :attr:`~.XDSMizer.save_pdf`.

  - :meth:`~.XDSMizer.monitor`
      - Rename ``latex_output`` to ``save_pdf``.
      - Rename ``outfilename`` to ``file_name`` and do not use suffix.
      - Rename ``outdir`` to ``directory_path``.

  - :meth:`~.XDSMizer.run`

      - Rename ``latex_output`` to ``save_pdf``.
      - Rename ``html_output`` to ``save_html``.
      - Rename ``json_output`` to ``save_json``.
      - Rename ``open_browser`` to ``show_html``.
      - Rename ``outfilename`` to ``file_name`` and do not use suffix.
      - Rename ``outdir`` to ``directory_path`` and use ``"."`` as default value.

- :meth:`.StudyAnalysis.generate_xdsm`

  - Rename ``latex_output`` to ``save_pdf``.
  - Rename ``open_browser`` to ``show_html``.
  - Rename ``output_dir`` to ``directory_path``.

- :meth:`.MDOCouplingStructure.plot_n2_chart`: rename ``open_browser`` to ``show_html``.
- :meth:`.N2HTML`: rename ``open_browser`` to ``show_html``.
- :func:`generate_n2_plot` rename ``open_browser`` to ``show_html``.
- :meth:`.Scenario.xdsmize`: rename ``print_statuses`` to ``log_workflow_status``.
- :meth:`.XDSMizer.monitor`: rename ``print_statuses`` to ``log_workflow_status``.
- Rename :attr:`.XDSMizer.print_statuses` to :attr:`.XDSMizer.log_workflow_status`.
- The CLI of the :class:`.StudyAnalysis` uses the shortcut ``-p`` for the option ``--save_pdf``.
